@extends ('admin.index')
@section('sub-header')
<div class="sub-header">
    <div class="d-flex align-items-center flex-wrap mr-auto">
        <h5 class="dashboard_bar">Cities</h5>
    </div>
    <div class="d-flex align-items-center">
        <a href="javascript:void(0);" onClick="jQuery('#formSingle').submit();" class="btn btn-xs btn-primary mr-1">Submit</a>
        <a href="{{ route('admin_cities') }}" class="btn btn-xs btn-light  mr-1">Cancel</a>
    </div>
</div>
@endsection ('sub-header');
@section('content')
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4></h4>
                    <span>Element</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin_cities') }}">Cities</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">

                        </a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        {!! Form::open(['method' => 'post', 'id' => 'formSingle', 'route' => ['admin_cities-save', $row_id]]) !!}
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Details</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Title<span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="title" class="form-control" placeholder="Title" value="{{ old('title') ?? $row->title }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label mt-0">Status<span class="text-danger"> *</span></label>
                                <div class="col-sm-9 mt-1">
                                    <label class="radio-inline mr-3">

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Content</label>
                                <div class="col-sm-9">

                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <a href="{{ route('admin_cities') }}" class="btn btn-light">
                                Cancel
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="action" id="input-action" class="form-control" placeholder="action"
                value="{{ $action }}">
        @csrf
        {!! Form::close() !!}
    </div>
</div>
@endsection ('content')
@section('script')
<link rel="stylesheet" href="{{ asset('assets/vendor/pickadate/themes/default.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/pickadate/themes/default.date.css') }}">
<script src="{{ asset('assets/vendor/pickadate/picker.js') }}"></script>
<script src="{{ asset('assets/vendor/pickadate/picker.date.js') }}"></script>
<script src="{{ asset('assets/js/plugins-init/pickadate-init.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/vendor/summernote/summernote.css') }}">
<script src="{{ asset('assets/vendor/summernote/js/summernote.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins-init/summernote-init.js') }}"></script>
@endsection ('script')