@extends ('admin.index')
@section('sub-header')
<div class="sub-header">
    <div class="d-flex align-items-center flex-wrap mr-auto">
        <h5 class="dashboard_bar mr-auto">Zipcodes</h5>
    </div>
    <a href="{{ route('admin_zipcodes-add') }}" class="btn btn-info btn-sm mr-3">+ Add Item</a>
</div>
@endsection ('sub-header');
@section('content')
<!--**********************************
                                                    Content body start
                                                ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Zipcodes Items Listing</h4>
                    <p class="mb-0">Your business dashboard template</p>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Zipcodes Items</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        {!! Form::open(['method' => 'get', 'id' => 'formBrowse']) !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Zipcodes Items</h4>
                        <div>
                            Order
                            @php
                            echo Form::select(
                            'order',
                            [
                            'title' => 'Title',
                            'id' => 'Id',
                            ],
                            $order,
                            ['id' => 'selectOrder', 'class' => 'btn dropdown-toggle text-left'],
                            );
                            @endphp
                            @php
                            echo Form::select(
                            'order_dir',
                            [
                            'asc' => 'Asc',
                            'desc' => 'Desc',
                            ],
                            $order_dir,
                            ['id' => 'selectOrderDir', 'class' => 'btn dropdown-toggle text-left'],
                            );
                            @endphp
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th style="width:50px;">
                                            <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                                <input type="checkbox" class="custom-control-input" id="checkAll" required="">
                                                <label class="custom-control-label" for="checkAll"></label>
                                            </div>
                                        </th>
                                        <th style="min-width: 200px;"><strong>TITLE</strong></th>
                                        <th><strong>Status</strong></th>
                                        <th><strong>Created</strong></th>
                                        <th><strong>ID.</strong></th>
                                        <th style="width: 20px"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($posts as $key => $row)
                                    <tr>
                                        <td>
                                        <td>{{ $posts->firstItem() + $key }}</td>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                                <input type="checkbox" class="custom-control-input" id="customCheckBox2" required="">
                                                <label class="custom-control-label" for="customCheckBox2"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin_zipcodes-edit', $row->id) }}">
                                                {{ $row->title }}
                                            </a>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="text-primary">

                                            </div>
                                        </td>
                                        <td><strong></strong></td>
                                        <td>
                                            <div class="d-flex action-button">
                                                <a class="btn btn-info btn-xs light px-2">
                                                    <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M17 3C17.2626 2.73735 17.5744 2.52901 17.9176 2.38687C18.2608 2.24473 18.6286 2.17157 19 2.17157C19.3714 2.17157 19.7392 2.24473 20.0824 2.38687C20.4256 2.52901 20.7374 2.73735 21 3C21.2626 3.26264 21.471 3.57444 21.6131 3.9176C21.7553 4.26077 21.8284 4.62856 21.8284 5C21.8284 5.37143 21.7553 5.73923 21.6131 6.08239C21.471 6.42555 21.2626 6.73735 21 7L7.5 20.5L2 22L3.5 16.5L17 3Z" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    </svg>
                                                </a>
                                                <a href="javascript:void(0);" class="ml-2 btn btn-xs px-2 light btn-danger">
                                                    <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M3 6H5H21" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                        <path d="M8 6V4C8 3.46957 8.21071 2.96086 8.58579 2.58579C8.96086 2.21071 9.46957 2 10 2H14C14.5304 2 15.0391 2.21071 15.4142 2.58579C15.7893 2.96086 16 3.46957 16 4V6M19 6V20C19 20.5304 18.7893 21.0391 18.4142 21.4142C18.0391 21.7893 17.5304 22 17 22H7C6.46957 22 5.96086 21.7893 5.58579 21.4142C5.21071 21.0391 5 20.5304 5 20V6H19Z" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    </svg>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="12" class="pagination2">
                                            <style>
                                                .pagination2 svg {
                                                    display: none;
                                                }
                                            </style>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<!--**********************************
                                                    Content body end
                                                ***********************************-->
@endsection ('content')