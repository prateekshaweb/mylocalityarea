<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('index');
});

/**
 * Admin Panel
 */

Route::group(['prefix' => 'admin', 'middleware' => ['web']], function () {

    // Route::get('/', function () {
    //     return view('admin.index');
    // });
    Route::get('/', [\App\Http\Controllers\Admin\DashboardController::class, 'index'])
        ->name('dashboard');

    /**
     * Cities
     */
    Route::prefix('admin_cities')->group(function () {

        Route::get('/', [\App\Http\Controllers\Admin\CitiesController::class, 'index'])->name('admin_cities');
        Route::get('/edit/{id}', [\App\Http\Controllers\Admin\CitiesController::class, 'edit'])->name('admin_cities-edit');
        Route::get('/add', [\App\Http\Controllers\Admin\CitiesController::class, 'add'])->name('admin_cities-add');
        Route::post('/edit', [\App\Http\Controllers\Admin\CitiesController::class, 'save'])->name('admin_cities-save');
        Route::post('/edit/{id}', [\App\Http\Controllers\Admin\CitiesController::class, 'save'])->name('admin_cities-save');
    });

    /**
     * Zipcodes
     */
    Route::prefix('admin_zipcodes')->group(function () {

        Route::get('/', [\App\Http\Controllers\Admin\ZipcodeController::class, 'index'])->name('admin_zipcodes');
        Route::get('/edit/{id}', [\App\Http\Controllers\Admin\ZipcodeController::class, 'edit'])->name('admin_zipcodes-edit');
        Route::get('/add', [\App\Http\Controllers\Admin\ZipcodeController::class, 'add'])->name('admin_zipcodes-add');
        Route::post('/edit', [\App\Http\Controllers\Admin\ZipcodeController::class, 'save'])->name('admin_zipcodes-save');
        Route::post('/edit/{id}', [\App\Http\Controllers\Admin\ZipcodeController::class, 'save'])->name('admin_zipcodes-save');
    });


    /**
     * Services
     */
    Route::prefix('admin_services')->group(function () {

        Route::get('/', [\App\Http\Controllers\Admin\ServicesController::class, 'index'])->name('admin_services');
        Route::get('/edit/{id}', [\App\Http\Controllers\Admin\ServicesController::class, 'edit'])->name('admin_services-edit');
        Route::get('/add', [\App\Http\Controllers\Admin\ServicesController::class, 'add'])->name('admin_services-add');
        Route::post('/edit', [\App\Http\Controllers\Admin\ServicesController::class, 'save'])->name('admin_services-save');
        Route::post('/edit/{id}', [\App\Http\Controllers\Admin\ServicesController::class, 'save'])->name('admin_services-save');
    });
});

Route::group(['middleware' => ['web']], function () {

    /**
     * Cities
     */
    Route::prefix('cities')->group(function () {

        Route::get('/', '\App\Http\Controllers\Cities@index')->name('cities');
        Route::get('/view', '\App\Http\Controllers\Cities@view')->name('cities-view');
        Route::get('/add', '\App\Http\Controllers\Cities@add')->name('cities-add');
        Route::get('/edit/{id}', '\App\Http\Controllers\Cities@edit')->name('cities-edit');
        Route::get('/delete', '\App\Http\Controllers\Cities@delete')->name('cities-delete');
    });

    /**
     * Services
     */
    Route::prefix('services')->group(function () {

        Route::get('/', '\App\Http\Controllers\Services@index')->name('services');
        Route::get('/view', '\App\Http\Controllers\Services@view')->name('services-view');
        Route::get('/add', '\App\Http\Controllers\Services@add')->name('services-add');
        Route::get('/edit/{id}', '\App\Http\Controllers\Services@edit')->name('services-edit');
        Route::get('/delete', '\App\Http\Controllers\Services@delete')->name('services-delete');
    });
});
