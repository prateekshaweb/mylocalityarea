jQuery(function() {

    // Add event to selectOrder
    jQuery('#selectOrder, #selectOrderDir, #filter_status, .filter_refresh').on('change', function() {
        // Form submit
        jQuery('#formBrowse').submit();
    });

    /**
     * Method to add file input
     * 
     * @author Sumeet Shroff
     */
    jQuery('#add-file-input-button').click(function(e) {

        // Prevent default
        e.preventDefault();

        // Add Input
        input = '<div><input name="filename[]" type="file" class="p-1"></div>';
        jQuery('#add-file-input').append(input);

        return false;
    });

})