(function($) {
    "use strict"

    //date picker classic default
    $('.datepicker-default').pickadate({
        format: 'yyyy-m-d',
    });

})(jQuery);