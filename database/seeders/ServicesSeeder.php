<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create('en_EN');

         $faker->addProvider(new \Faker\Provider\en_US\Address($faker));
 
         //
         for ($i = 0; $i < 30; $i++) {
 
             $first_name = $faker->firstName;
             $last_name = $faker->lastName;
 
             DB::table('services')->insert([
                 'services_list' => $faker->text($maxNbChars = 50),
                 'title' =>  $faker->text($maxNbChars = 50),
             ]);
         }
    }
}
