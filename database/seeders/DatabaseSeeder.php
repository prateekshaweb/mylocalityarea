<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CitiesSeeder::class);
        $this->call(ZipcodeSeeder::class);
        $this->call(LocalListingSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(ServicesIncludedSeeder::class);
    }
}
