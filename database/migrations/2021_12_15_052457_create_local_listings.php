<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalListings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_listings', function (Blueprint $table) {
            $table->id();
            $table->integer('zipcode_id');
            $table->string('title', 120);
            $table->string('description', 120);
            $table->string('address', 120);
            $table->string('logo', 120);
            $table->string('website', 120);
            $table->string('mobile', 120);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_listings');
    }
}
