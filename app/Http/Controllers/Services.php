<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Services extends Controller
{
    //
    public function index(Request $request, \App\Models\Services $services)
    {
        $posts = $services->get();
           // ->orderBy($order, $order_dir)
            //->paginate($posts_per_page);
    

        return view('services.default')->with('posts', $posts);
    }

    public function add()
    {
        return view('services.edit');
    }

    public function edit($id, \App\Models\Services $services)
    {
        //Sanitize
        $id = intval($id);

        $row = $services::find($id);

        return view('services.edit')->with(
            array(
                'row' => $row,
                'action' => 'edit'
            )
        );


        var_dump($id);
        echo 'nikhil-edit';
    }

    public function delete()
    {
        echo 'nikhil-delete';
    }

    public function view()
    {
        echo 'nikhil-view';
    }
}
