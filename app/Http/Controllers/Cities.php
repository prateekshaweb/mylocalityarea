<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Cities extends Controller
{
    //
    public function index(Request $request, \App\Models\Cities $cities)
    {
        $posts = $cities->get();
           // ->orderBy($order, $order_dir)
            //->paginate($posts_per_page);
    

        return view('cities.default')->with('posts', $posts);
    }

    public function add()
    {
        return view('cities.edit');
    }

    public function edit($id, \App\Models\Cities $cities)
    {
        //Sanitize
        $id = intval($id);

        $row = $cities::find($id);

        return view('cities.edit')->with(
            array(
                'row' => $row,
                'action' => 'edit'
            )
        );


        var_dump($id);
        echo 'nikhil-edit';
    }

    public function delete()
    {
        echo 'nikhil-delete';
    }

    public function view()
    {
        echo 'nikhil-view';
    }
}
