<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ZipcodeController extends Controller
{
    //
    public function index(Request $request, \App\Models\Zipcode $zipcode)
    {

        // Set the default order to title
        $order = $request->get('order', 'title');
        if (!in_array($order, $zipcode->allFields())) {
            $order = 'title';
        }

        // Set the order direction
        $order_dir = strtolower($request->get('order_dir', 'asc'));
        if (!in_array($order_dir, array('asc', 'desc'))) {
            $order_dir = 'asc';
        }


        // Posts per page
        $posts_per_page = intval($request->get('posts_per_page', '15'));


        // Get the posts
        $posts = $zipcode
            ->select('zipcode.*');

        return view('admin.zipcode.default')
            ->with('posts', $posts)
            ->with('posts_per_page', $posts_per_page)
            ->with('order', $order)
            ->with('order_dir', $order_dir);
    }

    /**
     * Method to show all the rows
     * 
     * @param   object  $zipcode   Zipcodes Model
     */
    public function add(Request $request, \App\Models\Zipcode $zipcode)
    {

        return view('admin.zipcode.edit')->with(
            array(
                'row' => $zipcode,
                'action' => 'add',
                'row_id' => 0
            )
        );
    }

    /**
     * Method to show all the rows
     * 
     * @param   object  $zipcode   Zipcodes Model
     */
    public function edit(Request $request, \App\Models\Zipcode $zipcode)
    {
        $id = intval($request->id);
        $row = $zipcode::find($id);

        return view('admin.zipcode.edit')->with(
            array(
                'row' => $row,
                'action' => 'edit',
                'row_id' => $id
            )
        );
    }

    /**
     * Method to show all the rows
     * 
     * @param   object  $zipcode   Zipcodes Model
     */
    public function save(Request $request, \App\Models\Zipcode $zipcode)
    {

        // Get the action
        $action = $request->action;

        // Data
        $data = $request->all();

        if ($action == 'edit') {
            $post = $zipcode->find($request->id);
            $post->update($data);
        } else {
            $post = $zipcode::create($data);
        }

        if (!is_null($post)) {

            if ($action == 'add') {
                return redirect()->route('admin_zipcode-edit', $post->id)
                    ->with('success', 'InvoiceItem created!.');
            }

            return back()->with("success", "Success! InvoiceItem updated");
        } else {
            return back()->with("failed", "Failed! Post not created or updated");
        }

        // On Success
        return redirect()->route('admin_zipcode-edit', $post->id)
            ->with('success', 'InvoiceItem Updated!.');
    }
}
