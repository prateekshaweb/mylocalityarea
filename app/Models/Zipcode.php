<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zipcode extends Model
{
    use HasFactory;
    protected $table = 'zipcode';

    protected $fillable = [
        'city_id',
        'title',
    ];

    public function allFields()
    {
        return $this->fillable;
    }

}
