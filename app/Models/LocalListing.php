<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalListing extends Model
{
    use HasFactory;
    protected $table = 'local_listings';

    protected $fillable = [
        'zipcode_id',
        'title',
        'description',
        'address',
        'logo',
        'website',
        'mobile',
    ];
}
