<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicesIncluded extends Model
{
    use HasFactory;
    protected $table = 'services_included';

    protected $fillable = [
        'service_id',
        'local_listing_id',
    ];
}
